package korchagin.as.theory_of_systems_and_systems_analysis;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by andrey on 21.03.17.
 */
public class LayoutManager extends LinearLayoutManager {

    private boolean canScroll = false;

    LayoutManager(Context context) {
        super(context);
    }

    LayoutManager(Context context, boolean canScroll) {
        super(context);
        this.canScroll = canScroll;
    }

    @Override
    public boolean canScrollHorizontally() {
        return canScroll;
    }

    @Override
    public boolean canScrollVertically() {
        return canScroll;
    }
}
