package korchagin.as.theory_of_systems_and_systems_analysis;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * Created by andrey on 22.03.17.
 */
public class HorizontalScroller extends HorizontalScrollView {

    private boolean scrollable = true;


    public HorizontalScroller(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalScroller(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setScrollable(boolean scrollable) {
        this.scrollable = scrollable;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return (scrollable && super.onInterceptTouchEvent(ev));
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return scrollable && super.onTouchEvent(ev);
    }
}
