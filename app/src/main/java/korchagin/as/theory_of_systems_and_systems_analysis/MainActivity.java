package korchagin.as.theory_of_systems_and_systems_analysis;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ViewTreeObserver.OnScrollChangedListener {

    private ArrayList<Point> points = new ArrayList<>();
    private Adapter adapter;
    private double dispersionSum;
    private double t[];
    private double a[];
    private double aIR[];
    private double ttabl = 2.1199;
    private double ft = 6.59;
    private double gt = 0.5157;
    private double s2b;
    private HorizontalScrollView valueScrollView;
    private HorizontalScroller titleScrollView;

    @Override
    public void onScrollChanged() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = (RecyclerView) findViewById(R.id.recycler_view);
        valueScrollView = (HorizontalScrollView) findViewById(R.id.value_scroll);
        valueScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                titleScrollView.setScrollX(valueScrollView.getScrollX());
            }
        });
        titleScrollView = ((HorizontalScroller) findViewById(R.id.title_scroll));
        titleScrollView.setScrollable(false);
        for (int i = 0; i < 8; i++) {
            points.add(new Point());
        }
        for (int i = 0; i < points.size(); i++) {
            if (i % 2 == 0) {
                points.get(i).setX1(-1);
            } else {
                points.get(i).setX1(1);
            }
        }
        for (int i = 0; i < points.size() / 2; i++) {
            if (i % 2 == 0) {
                points.get(i * 2).setX2(-1);
                points.get(i * 2 + 1).setX2(-1);
            } else {
                points.get(i * 2).setX2(1);
                points.get(i * 2 + 1).setX2(1);
            }
        }
        for (int i = 0; i < points.size() / 4; i++) {
            if (i % 4 == 0) {
                points.get(i * 4).setX3(-1);
                points.get(i * 4 + 1).setX3(-1);
                points.get(i * 4 + 2).setX3(-1);
                points.get(i * 4 + 3).setX3(-1);
            } else {
                points.get(i * 4).setX3(1);
                points.get(i * 4 + 1).setX3(1);
                points.get(i * 4 + 2).setX3(1);
                points.get(i * 4 + 3).setX3(1);
            }
        }
        for (Point point : points) {
            point.setX1X2(point.getX1() * point.getX2());
            point.setX1X3(point.getX1() * point.getX3());
            point.setX2X3(point.getX2() * point.getX3());
        }
        adapter = new Adapter(points);
        LayoutManager llm = new LayoutManager(getBaseContext());
        rv.setLayoutManager(llm);
        rv.setAdapter(adapter);
        Button calculate = (Button) findViewById(R.id.calculate);
        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                try {
                    getCurrentFocus().clearFocus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    for (Point point : points) {
                        point.setYi();
                        point.setDispersion();
                    }
                    checkKohren();
                    checkStudent();
                    checkFisher();
                    adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(), "Что-то пошло не так. Заполните все поля", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private double getMaxDispersion() {
        double max = 0;
        for (Point point : points) {
            if (point.getS2() > max) max = point.getS2();
        }
        return max;
    }


    private void checkKohren() {
        double maxDispersion = getMaxDispersion();
        dispersionSum = 0;
        TextView kohranResult = (TextView) findViewById(R.id.kohern_result);
        kohranResult.setText("");
        for (Point point : points) {
            dispersionSum += point.getS2();
        }
        double gp = maxDispersion / dispersionSum;
        kohranResult.setText(String.format("Gp = %s   Gt = %s   "
                        + ((gt > gp) ? "Gt > Gp, все построчные дисперсии однородны" :
                        "Gt < Gp, гипотеза отвергается"),
                String.format(Locale.ENGLISH, "%.4f", gp),
                String.format(Locale.ENGLISH, "%.4f", gt)));
        kohranResult.append("\nGt - табличное значение критерия Кохрена, а Gp - рассчетное");
    }

    private void checkStudent() {
        TextView studentResult = (TextView) findViewById(R.id.student_result);
        studentResult.setText("");
        double s2ak;
        a = new double[4];
        aIR = new double[4];
        for (int i = 0; i < points.size() / 2; i++) {
            for (Point point : points) {
                switch (i) {
                    case 0:
                        a[i] += point.getX0() * point.getYi();
                        aIR[i] += Double.parseDouble(point.getX1X2()) * point.getYi();
                        break;
                    case 1:
                        a[i] += point.getX1() * point.getYi();
                        aIR[i] += Double.parseDouble(point.getX1X3()) * point.getYi();
                        break;
                    case 2:
                        a[i] += point.getX2() * point.getYi();
                        aIR[i] += Double.parseDouble(point.getX2X3()) * point.getYi();
                        break;
                    case 3:
                        a[i] += point.getX3() * point.getYi();
                        break;
                    default:
                        Toast.makeText(getBaseContext(), "Wrong data", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
        for (int i = 0; i < 4; i++) a[i] /= 8;
        for (int i = 0; i < 3; i++) aIR[i] /= 8;

        studentResult.setText(String.format(Locale.ENGLISH, "a1 = %s\na2 = %s\na3 = %s\na4 = %s",
                String.format(Locale.ENGLISH, "%.4f", a[0]),
                String.format(Locale.ENGLISH, "%.4f", a[1]),
                String.format(Locale.ENGLISH, "%.4f", a[2]),
                String.format(Locale.ENGLISH, "%.4f", a[3])));
        String regressionEquation = "\nY' = " + String.format(Locale.ENGLISH,
                "%s*X0 " + ((a[1] > 0) ? " + " : " - ") + dblToString(a[1], 4, true) + "*X1" +
                        ((a[2] > 0) ? " + " : " - ") + dblToString(a[2], 4, true) + "*X2" +
                        ((a[3] > 0) ? " + " : " - ") + dblToString(a[3], 4, true) + "*X3",
                dblToString(a[0], 4, false));
        studentResult.append("\na1-a4 - коэффициенты уравнения регрессии");
        studentResult.append("\n\nУравнение регрессии: " + regressionEquation);

        studentResult.append("\n\nУравнение регрессии с учетом межфакторного взаимодействия:" + regressionEquation +
                ((aIR[0] > 0) ? " + " : " - ") + dblToString(aIR[1], 4, true) + "*X1X2" +
                ((aIR[1] > 0) ? " + " : " - ") + dblToString(aIR[1], 4, true) + "*X1X3" +
                ((aIR[2] > 0) ? " + " : " - ") + dblToString(aIR[2], 4, true) + "*X2X3\n\n");
        double sak;
        s2b = dispersionSum / 8;
        s2ak = s2b / (8 * 3);
        sak = Math.sqrt(s2ak);
        studentResult.append(String.format(Locale.ENGLISH, "S2B = %s\nS2ak = %s\nSak = %s",
                dblToString(s2b, 4, false),
                dblToString(s2ak, 4, false),
                dblToString(sak, 4, false)));
        studentResult.append("\nS2B - среднее арифметическое построчнх дисперсий," +
                " S2ak - дисперсия коэффициентов," +
                " Sak - среднеквадратическое отклонение погрешности определения коэффициенты");

        t = new double[a.length];

        for (int i = 0; i < a.length; i++) t[i] = Math.abs(a[i]) / sak;

        studentResult.append(String.format(Locale.ENGLISH, "\n\nttabl = %s\nt0 = %s\nt1 = %s\nt2 = %s\nt3 = %s",
                dblToString(ttabl, 4, false),
                dblToString(t[0], 4, false) + ((ttabl > (Math.abs(a[0]) / sak)) ? ", значит t" + Integer.toString(0) + " статистически незначителен" : ""),
                dblToString(t[1], 4, false) + ((ttabl > (Math.abs(a[1]) / sak)) ? ", значит t" + Integer.toString(1) + " статистически незначителен" : ""),
                dblToString(t[2], 4, false) + ((ttabl > (Math.abs(a[2]) / sak)) ? ", значит t" + Integer.toString(2) + " статистически незначителен" : ""),
                dblToString(t[3], 4, false) + ((ttabl > (Math.abs(a[3]) / sak)) ? ", значит t" + Integer.toString(3) + " статистически незначителен" : "")));
        studentResult.append("\nttabl - Табличное значение коэффициента Стьюдента, t0-t3 - отклонения от нуля");

        StringBuilder expression = new StringBuilder();
        expression.append("\n\nY' = ");
        for (int i = 0; i < t.length; i++) {
            if (ttabl <= t[i]) {
                expression.append(getSign(a[i]) + dblToString(a[i], 4, true) + "*X" + Integer.toString(i));
            }
        }
        studentResult.append(expression.toString());

    }

    private void checkFisher() {
        TextView fisherResult = (TextView) findViewById(R.id.fisher_result);
        fisherResult.setText("");
        double x[];
        double ysh[];
        x = new double[4];
        ysh = new double[points.size()];
        for (int i = 0; i < ysh.length; i++) {
            ysh[i] = 0;
        }
        int counter = 0;
        for (Point point : points) {
            double sum = 0;
            x[0] = point.getX0();
            x[1] = point.getX1();
            x[2] = point.getX2();
            x[3] = point.getX3();
            for (int i = 0; i < 4; i++) {
                if (t[i] >= ttabl) sum += x[i] * a[i];
            }
            ysh[counter] = sum;
            counter++;
        }
        for (int i = 0; i < ysh.length; i++) {
            fisherResult.append(String.format(Locale.ENGLISH, "Y' %d = ", i + 1) + dblToString(ysh[i], 4, false) + "\n");
        }

        double sum = 0;
        double s2ad;
        double fp;

        for (int i = 0; i < points.size(); i++) {
            sum += Math.pow((points.get(i).getYi() - ysh[i]), 2);
        }

        s2ad = (sum * 3) / (8 - 3);
        fp = s2ad / s2b;

        fisherResult.append(String.format(Locale.ENGLISH, "\n\nS2ad = %s\n\nFp = %s\nFt = %s" +
                        ((ft > fp) ? " > Fp => модель адекватна эксперементальным данным" : "<= Fp => модель адекватна эксперементальным данным"),
                dblToString(s2ad, 4, false),
                dblToString(fp, 4, false),
                dblToString(ft, 2, false)));
        fisherResult.append("\nS2ad - дисперсия адекватности, Fp и Ft - рассчетное и табличное значения коэффициента Фишера соответственно");
    }

    private String dblToString(double value, int chars, boolean abs) {
        if (abs) {
            return String.format(Locale.ENGLISH, "%." + String.format(Locale.ENGLISH, "%s", chars) + "f", Math.abs(value));
        } else {
            return String.format(Locale.ENGLISH, "%." + String.format(Locale.ENGLISH, "%s", chars) + "f", value);
        }
    }

    private String getSign(double value) {
        if (value < 0) {
            return " - ";
        } else {
            return " + ";
        }
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
