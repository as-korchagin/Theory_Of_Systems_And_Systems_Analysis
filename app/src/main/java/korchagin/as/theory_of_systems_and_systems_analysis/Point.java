package korchagin.as.theory_of_systems_and_systems_analysis;

/**
 * Created by andrey on 16.03.17.
 */
public class Point {
    private final double X0;
    private double X1;
    private double X2;
    private double X3;
    private double Y1;
    private double Y2;
    private double Y3;
    private double Yi;
    private double S2;
    private double X1X2;
    private double X1X3;
    private double X2X3;

    public Point() {
        X0 = 1;
        Yi = 0;
        S2 = 0;
    }


    public void setYi() {
        Yi = (Y1 + Y2 + Y3) / 3;
    }

    public void setDispersion() {
        S2 = Math.pow(Yi - Y1, 2);
        S2 += Math.pow(Yi - Y2, 2);
        S2 += Math.pow(Yi - Y3, 2);
        S2 /= 2;
    }

    public double getX0() {
        return X0;
    }

    public double getX1() {
        return X1;
    }

    public void setX1(double x1) {
        X1 = x1;
    }

    public double getX2() {
        return X2;
    }

    public void setX2(double x2) {
        X2 = x2;
    }

    public double getX3() {
        return X3;
    }

    public void setX3(double x3) {
        X3 = x3;
    }

    public double getY1() {
        return Y1;
    }

    public void setY1(double y1) {
        Y1 = y1;
    }

    public double getY2() {
        return Y2;
    }

    public void setY2(double y2) {
        Y2 = y2;
    }

    public double getY3() {
        return Y3;
    }

    public void setY3(double y3) {
        Y3 = y3;
    }

    public double getYi() {
        return Yi;
    }

    public double getS2() {
        return S2;
    }

    public String getX1X2() {
        return Double.toString(X1X2);
    }

    public void setX1X2(double x1X2) {
        X1X2 = x1X2;
    }

    public String getX1X3() {
        return Double.toString(X1X3);
    }

    public void setX1X3(double x1X3) {
        X1X3 = x1X3;
    }

    public String getX2X3() {
        return Double.toString(X2X3);
    }

    public void setX2X3(double x2X3) {
        X2X3 = x2X3;
    }

    public String getX0String() {
        return Double.toString(X0);
    }

    public String getX1String() {
        return Double.toString(X1);
    }

    public String getX2String() {
        return Double.toString(X2);
    }

    public String getX3String() {
        return Double.toString(X3);
    }
}
