package korchagin.as.theory_of_systems_and_systems_analysis;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by andrey on 16.03.17.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.PointViewHolder> {

    ArrayList<Point> points;

    public Adapter(ArrayList<Point> points) {
        this.points = points;
    }

    @Override
    public void onBindViewHolder(PointViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public PointViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.table_row, parent, false);
        return new PointViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final PointViewHolder holder, final int position) {
        FocusChangeListener focusChangeListener = new FocusChangeListener(position);
        holder.X0.setText(points.get(position).getX0String());
        holder.X1.setText(points.get(position).getX1String());
        holder.X2.setText(points.get(position).getX2String());
        holder.X3.setText(points.get(position).getX3String());
        holder.X1X3.setText(points.get(position).getX1X2());
        holder.X2X3.setText(points.get(position).getX1X3());
        holder.X1X2.setText(points.get(position).getX2X3());
        if (points.get(position).getY1() != 0)
            holder.Y1.setText(String.format(Locale.ENGLISH, "%.3f", points.get(position).getY1()));
        if (points.get(position).getY2() != 0)
            holder.Y2.setText(String.format(Locale.ENGLISH, "%.3f", points.get(position).getY2()));
        if (points.get(position).getY3() != 0)
            holder.Y3.setText(String.format(Locale.ENGLISH, "%.3f", points.get(position).getY3()));
        holder.Y2.setOnFocusChangeListener(focusChangeListener);
        holder.Y3.setOnFocusChangeListener(focusChangeListener);
        holder.Y1.setOnFocusChangeListener(focusChangeListener);
        holder.Yi.setText(String.format(Locale.ENGLISH, "%.3f", points.get(position).getYi()));
        holder.S2.setText(String.format(Locale.ENGLISH, "%.3f", points.get(position).getS2()));
    }

    @Override
    public int getItemCount() {
        return points.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PointViewHolder extends RecyclerView.ViewHolder {
        LinearLayout row;
        TextView X0;
        TextView X1;
        TextView X2;
        TextView X3;
        TextView X1X2;
        TextView X2X3;
        TextView X1X3;
        EditText Y1;
        EditText Y2;
        EditText Y3;
        TextView Yi;
        TextView S2;

        public PointViewHolder(final View itemView) {
            super(itemView);
            row = (LinearLayout) itemView.findViewById(R.id.row);
            X0 = (TextView) itemView.findViewById(R.id.x0);
            X1 = (TextView) itemView.findViewById(R.id.x1);
            X2 = (TextView) itemView.findViewById(R.id.x2);
            X3 = (TextView) itemView.findViewById(R.id.x3);
            X1X2 = (TextView) itemView.findViewById(R.id.x1x2);
            X1X3 = (TextView) itemView.findViewById(R.id.x1x3);
            X2X3 = (TextView) itemView.findViewById(R.id.x2x3);
            Y1 = (EditText) itemView.findViewById(R.id.y1);
            Y2 = (EditText) itemView.findViewById(R.id.y2);
            Y3 = (EditText) itemView.findViewById(R.id.y3);
            Yi = (TextView) itemView.findViewById(R.id.yi);
            S2 = (TextView) itemView.findViewById(R.id.s2);
        }
    }


    private class FocusChangeListener implements View.OnFocusChangeListener {
        int position;

        public FocusChangeListener(int position) {
            this.position = position;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            EditText editText = (EditText) v;
            if (!hasFocus && !editText.getText().toString().equals("")) {
                int id = v.getId();
                try {
                    if (Double.parseDouble(editText.getText().toString()) == 0) {
                        ((EditText) v).getText().clear();
                    } else {
                        switch (id) {
                            case R.id.y1:
                                points.get(position).setY1(Double.parseDouble(editText.getText().toString()));
                                break;
                            case R.id.y2:
                                points.get(position).setY2(Double.parseDouble(editText.getText().toString()));
                                break;
                            case R.id.y3:
                                points.get(position).setY3(Double.parseDouble(editText.getText().toString()));
                                break;
                            default:
                                Log.d("Logs", "onTouch Error");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
